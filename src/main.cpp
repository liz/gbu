#include <Arduino.h>

// The Plan:
// - create MIDI handler
// - add USB Override Switch
// - add MIDI Protocol handlers (BT, USB, WiFi, Wired)
// - Add Audio Output
// - MIDI Inputs
//  - Accelerometer / Gyroscope
//  - Trigger Switch
//  - Barrel Light Sensor?

void setup() {
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly:
}
